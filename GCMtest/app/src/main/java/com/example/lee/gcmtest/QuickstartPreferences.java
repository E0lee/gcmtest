package com.example.lee.gcmtest;

/**
 * Created by lee on 2015/7/17.
 */
public class QuickstartPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

}