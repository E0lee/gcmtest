package com.example.lee.gcmtest;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by lee on 2015/7/17.
 */
public class BootBroadcastReceiver extends BroadcastReceiver{
    static final String ACTION = "android.intent.action.BOOT_COMPLETED";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    Activity act;
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "get boot action", Toast.LENGTH_LONG).show();

        if (intent.getAction().equals(ACTION) && checkPlayServices(context)){
            /*Intent Intent=new Intent(context,RegistrationIntentService.class);
            Intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(Intent);*/
            Intent startServiceIntent = new Intent(context, RegistrationIntentService.class);
            context.startService(startServiceIntent);
            startServiceIntent = new Intent(context, RegistrationIntentService.class);
            context.startService(startServiceIntent);
        }
    }
    private boolean checkPlayServices(Context context) {
        int resultCode;
        resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode,act,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");

            }
            return false;
        }
        return true;
    }
}
